﻿/*
Navicat MySQL Data Transfer

Source Server         : localhost:wky
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : studentsystem

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2014-01-08 23:14:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `college`
-- ----------------------------
DROP TABLE IF EXISTS `college`;
CREATE TABLE `college` (
  `college_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`college_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of college
-- ----------------------------
INSERT INTO college VALUES ('1', '信息科学与工程学院');
INSERT INTO college VALUES ('2', '软件学院');
INSERT INTO college VALUES ('3', '商学院');
INSERT INTO college VALUES ('4', '理学院');
INSERT INTO college VALUES ('5', '文学院');
INSERT INTO college VALUES ('6', '政治学与国际关系学院');
INSERT INTO college VALUES ('7', '法学院');
INSERT INTO college VALUES ('8', '民族学与社会学学院');
INSERT INTO college VALUES ('9', '体育与健康科学学院');
INSERT INTO college VALUES ('10', '外国语学院');
INSERT INTO college VALUES ('11', '化学化工学院');
INSERT INTO college VALUES ('12', '管理学院');
INSERT INTO college VALUES ('13', '艺术学院');
INSERT INTO college VALUES ('14', '国际教育学院');
INSERT INTO college VALUES ('15', '人民武装学院');
INSERT INTO college VALUES ('16', '预科教育学院');
INSERT INTO college VALUES ('17', '相思湖学院');
INSERT INTO college VALUES ('18', '海洋与生物技术学院');

-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `cou_id` int(11) NOT NULL AUTO_INCREMENT,
  `cou_name` varchar(50) DEFAULT NULL,
  `cou_remark` varchar(500) DEFAULT NULL,
  `cou_credit` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`cou_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO course VALUES ('1', 'JAVA', null, '3.50');
INSERT INTO course VALUES ('2', '数据结构', null, '3.50');
INSERT INTO course VALUES ('3', '高等数学(上)', null, '3.50');
INSERT INTO course VALUES ('4', '高等数学(下)', null, '3.50');
INSERT INTO course VALUES ('5', '计算机英语', null, '2.00');
INSERT INTO course VALUES ('6', 'C++', null, '3.00');
INSERT INTO course VALUES ('7', '公共体育', null, '2.00');
INSERT INTO course VALUES ('8', '数据库概论', null, '3.00');
INSERT INTO course VALUES ('9', '数据库系统', null, '3.00');
INSERT INTO course VALUES ('10', '线性代数', '', '1.00');

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `stu_id` int(11) NOT NULL AUTO_INCREMENT,
  `stu_name` varchar(50) DEFAULT NULL,
  `stu_sex` varchar(1) DEFAULT NULL,
  `stu_age` int(11) DEFAULT NULL,
  `stu_birthday` date DEFAULT NULL,
  `stu_class` varchar(50) DEFAULT NULL,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`stu_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO student VALUES ('1', '李世全', '0', '20', '1988-09-18', '1', 'lishiquan', '123456');
INSERT INTO student VALUES ('2', '杨泽川', '1', '20', '1991-06-21', '1', 'yangzechuan', '123456');
INSERT INTO student VALUES ('3', '印度阿三', '0', '21', '1988-07-24', null, 'yinduasan', '123456');
INSERT INTO student VALUES ('4', '艾军', '0', '22', '1993-01-23', '软件工程', 'aijun', '123456');

-- ----------------------------
-- Table structure for `stu_cou`
-- ----------------------------
DROP TABLE IF EXISTS `stu_cou`;
CREATE TABLE `stu_cou` (
  `stu_cou_id` int(11) NOT NULL AUTO_INCREMENT,
  `stu_id` int(11) NOT NULL,
  `cou_id` int(11) NOT NULL,
  `tea_id` int(11) NOT NULL,
  `score` double DEFAULT NULL,
  `socre` double DEFAULT NULL,
  PRIMARY KEY (`stu_cou_id`),
  KEY `FK_fd2i9jhyx88ktcbudlprem9y0` (`cou_id`),
  KEY `FK_k71umpbvqf7722foi4l5d97g9` (`stu_id`),
  KEY `FK_23h594dgw9kb13yc6i4ekt41f` (`tea_id`),
  CONSTRAINT `FK_23h594dgw9kb13yc6i4ekt41f` FOREIGN KEY (`tea_id`) REFERENCES `teacher` (`tea_id`),
  CONSTRAINT `FK_fd2i9jhyx88ktcbudlprem9y0` FOREIGN KEY (`cou_id`) REFERENCES `course` (`cou_id`),
  CONSTRAINT `FK_k71umpbvqf7722foi4l5d97g9` FOREIGN KEY (`stu_id`) REFERENCES `student` (`stu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu_cou
-- ----------------------------
INSERT INTO stu_cou VALUES ('1', '1', '1', '1', '90', null);
INSERT INTO stu_cou VALUES ('2', '1', '2', '1', '91', null);
INSERT INTO stu_cou VALUES ('3', '1', '3', '1', '92', null);
INSERT INTO stu_cou VALUES ('4', '1', '4', '1', '93', null);
INSERT INTO stu_cou VALUES ('5', '1', '5', '1', '94', null);
INSERT INTO stu_cou VALUES ('6', '1', '6', '1', '95', null);
INSERT INTO stu_cou VALUES ('7', '1', '7', '1', '96', null);
INSERT INTO stu_cou VALUES ('8', '1', '8', '1', '97', null);
INSERT INTO stu_cou VALUES ('9', '1', '9', '1', '98', null);
INSERT INTO stu_cou VALUES ('10', '2', '1', '2', '60', null);
INSERT INTO stu_cou VALUES ('11', '2', '2', '2', '61', null);
INSERT INTO stu_cou VALUES ('12', '2', '3', '2', '62', null);
INSERT INTO stu_cou VALUES ('13', '2', '4', '2', '63', null);
INSERT INTO stu_cou VALUES ('14', '2', '5', '2', '64', null);
INSERT INTO stu_cou VALUES ('15', '2', '6', '2', '65', null);
INSERT INTO stu_cou VALUES ('16', '2', '7', '2', '66', null);
INSERT INTO stu_cou VALUES ('17', '2', '8', '2', '67', null);
INSERT INTO stu_cou VALUES ('18', '2', '9', '2', '68', null);
INSERT INTO stu_cou VALUES ('20', '1', '1', '2', '70', null);
INSERT INTO stu_cou VALUES ('21', '1', '2', '2', '71', null);
INSERT INTO stu_cou VALUES ('22', '1', '3', '2', '72', null);
INSERT INTO stu_cou VALUES ('23', '1', '4', '2', '73', null);
INSERT INTO stu_cou VALUES ('24', '1', '5', '2', '74', null);
INSERT INTO stu_cou VALUES ('25', '1', '6', '2', '75', null);
INSERT INTO stu_cou VALUES ('26', '1', '7', '2', '76', null);
INSERT INTO stu_cou VALUES ('27', '1', '8', '2', '77', null);
INSERT INTO stu_cou VALUES ('28', '1', '9', '2', '78', null);
INSERT INTO stu_cou VALUES ('29', '2', '1', '1', '80', null);
INSERT INTO stu_cou VALUES ('30', '2', '2', '1', '81', null);
INSERT INTO stu_cou VALUES ('31', '2', '3', '1', '82', null);
INSERT INTO stu_cou VALUES ('32', '2', '4', '1', '83', null);
INSERT INTO stu_cou VALUES ('33', '2', '5', '1', '84', null);
INSERT INTO stu_cou VALUES ('34', '2', '6', '1', '85', null);
INSERT INTO stu_cou VALUES ('35', '2', '7', '1', '86', null);
INSERT INTO stu_cou VALUES ('36', '2', '8', '1', '87', null);
INSERT INTO stu_cou VALUES ('37', '2', '9', '1', '88', null);

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `tea_id` int(11) NOT NULL AUTO_INCREMENT,
  `tea_name` varchar(50) DEFAULT NULL,
  `tea_sex` varchar(1) DEFAULT NULL,
  `tea_age` int(11) DEFAULT NULL,
  `tea_birthday` date DEFAULT NULL,
  `tea_college` varchar(50) DEFAULT NULL,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`tea_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO teacher VALUES ('1', '刘美玲', '1', '18', '1983-06-14', '1', 'liumeiling', '654321');
INSERT INTO teacher VALUES ('2', '何建强', '0', '40', '1973-12-06', '1', 'hejianqiang', '654321');
INSERT INTO teacher VALUES ('3', '张桂芬', '1', '34', '1983-06-14', null, 'zhangguifen', '654321');
INSERT INTO teacher VALUES ('4', '曲良东', '0', '36', '1978-01-01', '软件学院', 'quliangdong', '654321');
