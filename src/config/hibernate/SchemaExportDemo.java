package config.hibernate;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class SchemaExportDemo {

	public static void main(String[] args) {
		Configuration cfg = new Configuration().configure("config/hibernate/hibernate.cfg.xml");
		SchemaExport se = new SchemaExport(cfg);
		se.create(true, true);
	}

}
