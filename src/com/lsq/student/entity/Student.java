/**
 * Student.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lsq.common.entity.AllEntity;
import com.lsq.common.util.DateUtil;

/**
 * @author Lisq
 */
@Entity(name = "Student")
@Table(name = "student")
public class Student extends AllEntity<Student> {

	private static final long serialVersionUID = 7301215176157455573L;
	
	private Integer stuId;
	private String stuName;
	private String stuSex;
	private Integer stuAge;
	private Date stuBirthday;
	private String stuClass;
	private String userName;
	private String password;

	public Student() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "stu_id", nullable = false, insertable = false, updatable = false)
	public Integer getStuId() {
		return this.stuId;
	}

	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}

	@Column(name = "stu_name", length = 50)
	public String getStuName() {
		return this.stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	@Column(name = "stu_sex", length = 1)
	public String getStuSex() {
		return this.stuSex;
	}

	public void setStuSex(String stuSex) {
		this.stuSex = stuSex;
	}

	@Column(name = "stu_age")
	public Integer getStuAge() {
		return this.stuAge;
	}

	public void setStuAge(Integer stuAge) {
		this.stuAge = stuAge;
	}

	@Column(name = "stu_birthday")
	@Temporal(TemporalType.DATE)
	public Date getStuBirthday() {
		return this.stuBirthday;
	}

	public void setStuBirthday(Date stuBirthday) {
		this.stuBirthday = stuBirthday;
	}

	@Column(name = "stu_class", length = 50)
	public String getStuClass() {
		return stuClass;
	}

	public void setStuClass(String stuClass) {
		this.stuClass = stuClass;
	}

	@Column(name = "user_name", length = 20)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "password", length = 20)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String formatDate() {
		if (this.stuBirthday != null) {
			return DateUtil.DATE_FORMAT_YMD.format(this.stuBirthday).toString();
		} else {
			return null;
		}
	}

}