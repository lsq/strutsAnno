/**
 * StudentAction.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.lsq.common.action.BaseAction;
import com.lsq.student.command.StudentQueryInfo;
import com.lsq.student.entity.Student;
import com.lsq.student.service.IStudentService;

/**
 * @author Lisq
 */
@Namespace("/stusys")
@Controller("studentAction")
@Scope(value="prototype")
public class StudentAction extends BaseAction{

	private static final long serialVersionUID = -4436850933436607106L;

	private Student student = new Student();
	private StudentQueryInfo info = new StudentQueryInfo();
	
	@Autowired
	private IStudentService studentService;
	
	@Action(value="toUpdateStudent" , results={@Result(name=SUCCESS, type="velocity", location="/WEB-INF/student/updateStudent.vm")})
	public String toUpdateStudent() {
		student = studentService.findStudent(info);
		return SUCCESS;
	}

	@Action(value="updateStudent" , results={@Result(name=SUCCESS, type="redirectAction", location="findStudentScorePage")})
	public String updateStudent() {
		studentService.updateStudent(student);
		return SUCCESS;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * @return the info
	 */
	public StudentQueryInfo getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(StudentQueryInfo info) {
		this.info = info;
	}

	
}
