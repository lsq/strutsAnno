/**
 * StudentServiceImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.student.command.StudentQueryInfo;
import com.lsq.student.dao.IStudentDao;
import com.lsq.student.entity.Student;
import com.lsq.student.service.IStudentService;

/**
 * @author Lisq
 * @param <T>
 */
@Service(value = "studentService")
public class StudentServiceImpl implements IStudentService{

	@Autowired
	private IStudentDao studentDao;

	public void updateStudent(Student student) {
		studentDao.updateStudent(student);
	}

	public Student findStudent(StudentQueryInfo info) {
		return studentDao.findStudent(info);
	}
	
}
