/**
 * IStudentService.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.service;

import com.lsq.student.command.StudentQueryInfo;
import com.lsq.student.entity.Student;

/**
 * @author Lisq
 */
public interface IStudentService {

	void updateStudent(Student student);

	Student findStudent(StudentQueryInfo info);

}
