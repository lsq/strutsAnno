/**
 * IStudentDao.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.dao;

import com.lsq.common.dao.IBaseDao;
import com.lsq.student.command.StudentQueryInfo;
import com.lsq.student.entity.Student;

public interface IStudentDao extends IBaseDao<Student, Integer> {

	void updateStudent(Student student);

	Student findStudent(StudentQueryInfo info);
}
