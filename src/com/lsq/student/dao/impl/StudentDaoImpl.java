/**
 * StudentDaoImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.student.dao.impl;

import org.springframework.stereotype.Repository;

import com.lsq.common.dao.impl.BaseDaoImpl;
import com.lsq.student.command.StudentQueryInfo;
import com.lsq.student.dao.IStudentDao;
import com.lsq.student.entity.Student;

/**
 * @author Lisq
 */
@Repository(value = "studentDao")
public class StudentDaoImpl extends BaseDaoImpl<Student, Integer> implements
		IStudentDao {

	public void updateStudent(Student student) {
		this.save(student);
	}

	public Student findStudent(StudentQueryInfo info) {
		return this.load(Student.class, info.getStuId());
	}

}
