/**
 * StuCouAction.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.lsq.common.action.BaseAction;
import com.lsq.common.page.Page;
import com.lsq.stucou.command.StuCouQueryInfo;
import com.lsq.stucou.entity.StuCou;
import com.lsq.stucou.service.IStuCouService;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */
@Namespace("/stusys")
@Controller("stuCouAction")
@Scope(value = "prototype")
public class StuCouAction extends BaseAction {

	private static final long serialVersionUID = -4436850933436607106L;

	private Student currentStu = new Student();
	private Teacher currentTea = new Teacher();
	private StuCouQueryInfo info = new StuCouQueryInfo();
	private Page<StuCou> stuCous;
	private StuCou stuCou;

	@Autowired
	private IStuCouService stuCouService;

	@Action(value = "findStudentScorePage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/student/findStudentScorePage.vm") })
	public String findStudentScorePage() {
		currentStu = (Student) session.get("currentUser");
		info.setStuId(currentStu.getStuId());
		stuCous = stuCouService.findStudentScorePage(info);
		return SUCCESS;
	}

	@Action(value = "findTeacherScorePage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/teacher/findTeacherScorePage.vm") })
	public String findTeacherScorePage() {
		currentTea = (Teacher) session.get("currentUser");
		info.setTeaId(currentTea.getTeaId());
		stuCous = stuCouService.findTeacherScorePage(info);
		return SUCCESS;
	}

	@Action(value = "toUpdateStuCou", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/teacher/updateStuCou.vm") })
	public String toUpdateStuCou() {
		stuCou = stuCouService.findStuCou(info);
		return SUCCESS;
	}
	
	@Action(value = "updateStuCou", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findTeacherScorePage") })
	public String updateStuCou() {
		stuCouService.updateStuCou(stuCou);
		return SUCCESS;
	}

	/**
	 * @return the currentStu
	 */
	public Student getCurrentStu() {
		return currentStu;
	}

	/**
	 * @param currentStu
	 *            the currentStu to set
	 */
	public void setCurrentStu(Student currentStu) {
		this.currentStu = currentStu;
	}

	/**
	 * @return the stuCous
	 */
	public Page<StuCou> getStuCous() {
		return stuCous;
	}

	/**
	 * @param stuCous
	 *            the stuCous to set
	 */
	public void setStuCous(Page<StuCou> stuCous) {
		this.stuCous = stuCous;
	}

	/**
	 * @return the info
	 */
	public StuCouQueryInfo getInfo() {
		return info;
	}

	/**
	 * @param info
	 *            the info to set
	 */
	public void setInfo(StuCouQueryInfo info) {
		this.info = info;
	}

	/**
	 * @return the currentTea
	 */
	public Teacher getCurrentTea() {
		return currentTea;
	}

	/**
	 * @param currentTea
	 *            the currentTea to set
	 */
	public void setCurrentTea(Teacher currentTea) {
		this.currentTea = currentTea;
	}

	/**
	 * @return the stuCou
	 */
	public StuCou getStuCou() {
		return stuCou;
	}

	/**
	 * @param stuCou
	 *            the stuCou to set
	 */
	public void setStuCou(StuCou stuCou) {
		this.stuCou = stuCou;
	}

}
