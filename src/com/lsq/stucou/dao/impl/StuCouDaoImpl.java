/**
 * StuCouDaoImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.lsq.common.command.BaseQueryInfo;
import com.lsq.common.dao.impl.BaseDaoImpl;
import com.lsq.common.page.Page;
import com.lsq.common.util.Tools;
import com.lsq.stucou.command.StuCouQueryInfo;
import com.lsq.stucou.dao.IStuCouDao;
import com.lsq.stucou.entity.StuCou;

/**
 * @author Lisq
 */
@Repository(value = "stuCouDao")
public class StuCouDaoImpl extends BaseDaoImpl<StuCou, Integer> implements
		IStuCouDao {

	private Page<StuCou> getMyPage(BaseQueryInfo info, String hql) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<StuCou> data = this.executeSql(hql);
			return this.putDataToPage(data);
		} else {
			return super.find(hql, info.getPageNumber().intValue(), info
					.getPageSize().intValue());
		}
	}

	private Page<StuCou> getMyPage(BaseQueryInfo info, String hql,
			Object[] condition) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<StuCou> data = this.executeSql(hql, condition);
			return this.putDataToPage(data);
		} else {
			return super.find(hql, condition, info.getPageNumber().intValue(),
					info.getPageSize().intValue());
		}
	}

	public Page<StuCou> findStudentScorePage(StuCouQueryInfo info) {
		Object[] values = new Object[50];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(StuCou.class.getName())
				.append(" as sc where 1=1 ");

		if (info.getStuId() != null) {
			hql.append(" and sc.student.stuId = ? ");
			values[idx++] = info.getStuId();
		}

		if (info.getTeaId() != null) {
			hql.append(" and sc.teacher.teaId = ? ");
			values[idx++] = info.getTeaId();
		}

		if (Tools.isNotEmptyString(info.getCouName())) {
			hql.append(" and sc.course.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}

		if (Tools.isNotEmptyString(info.getTeaName())) {
			hql.append(" and sc.teacher.teaName like ? ");
			values[idx++] = "%" + info.getTeaName() + "%";
		}

		if (info.getScoreFront() != null) {
			hql.append(" and sc.score >= ? ");
			values[idx++] = info.getScoreFront();
		}

		if (info.getScoreBack() != null) {
			hql.append(" and sc.score <= ? ");
			values[idx++] = info.getScoreBack();
		}

		hql.append(" order by sc.course.couId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return this.getMyPage(info, hql.toString(), condition);
		}
		return this.getMyPage(info, hql.toString());
	}

	public Page<StuCou> findTeacherScorePage(StuCouQueryInfo info) {
		Object[] values = new Object[50];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(StuCou.class.getName())
				.append(" as sc where 1=1 ");

		if (info.getStuId() != null) {
			hql.append(" and sc.student.stuId = ? ");
			values[idx++] = info.getStuId();
		}

		if (info.getTeaId() != null) {
			hql.append(" and sc.teacher.teaId = ? ");
			values[idx++] = info.getTeaId();
		}

		if (Tools.isNotEmptyString(info.getCouName())) {
			hql.append(" and sc.course.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}

		if (Tools.isNotEmptyString(info.getStuName())) {
			hql.append(" and sc.student.stuName like ? ");
			values[idx++] = "%" + info.getStuName() + "%";
		}

		if (info.getScoreFront() != null) {
			hql.append(" and sc.score >= ? ");
			values[idx++] = info.getScoreFront();
		}

		if (info.getScoreBack() != null) {
			hql.append(" and sc.score <= ? ");
			values[idx++] = info.getScoreBack();
		}

		hql.append(" order by sc.course.couId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<StuCou>) this.getMyPage(info, hql.toString(),
					condition);
		}
		return (Page<StuCou>) this.getMyPage(info, hql.toString());
	}

	@Override
	public StuCou findStuCou(StuCouQueryInfo info) {
		return this.load(StuCou.class, info.getStuCouId());
	}

	@Override
	public void updateStuCou(StuCou stuCou) {
		this.save(stuCou);
	}

}
