/**
 * IStuCouDao.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.dao;

import com.lsq.common.dao.IBaseDao;
import com.lsq.common.page.Page;
import com.lsq.stucou.command.StuCouQueryInfo;
import com.lsq.stucou.entity.StuCou;

public interface IStuCouDao extends IBaseDao<StuCou, Integer> {
	Page<StuCou> findStudentScorePage(StuCouQueryInfo info);

	Page<StuCou> findTeacherScorePage(StuCouQueryInfo info);

	StuCou findStuCou(StuCouQueryInfo info);

	void updateStuCou(StuCou stuCou);
}
