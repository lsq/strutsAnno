/**
 * StuCou.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.lsq.common.entity.AllEntity;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */
@Entity(name = "StuCou")
@Table(name = "stu_cou")
public class StuCou extends AllEntity<StuCou> {

	private static final long serialVersionUID = 312505589457993686L;
	
	private Integer stuCouId;
	private Double score;
	private Course course;
	private Student student;
	private Teacher teacher;

	public StuCou() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "stu_cou_id", nullable = false, insertable = false, updatable = false)
	public Integer getStuCouId() {
		return stuCouId;
	}

	public void setStuCouId(Integer stuCouId) {
		this.stuCouId = stuCouId;
	}

	@Column(name = "socre")
	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@ManyToOne(targetEntity = Course.class)
	@JoinColumn(name = "cou_id", insertable = false, updatable = false)
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@ManyToOne(targetEntity = Student.class)
	@JoinColumn(name = "stu_id", insertable = false, updatable = false)
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@ManyToOne(targetEntity = Teacher.class)
	@JoinColumn(name = "tea_id", insertable = false, updatable = false)
	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
