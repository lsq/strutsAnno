/**
 * Course.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lsq.common.entity.AllEntity;

/**
 * @author Lisq
 */
@Entity(name = "Course")
@Table(name = "course")
public class Course extends AllEntity<Course> {

	private static final long serialVersionUID = 6173902017253168467L;
	
	private Integer couId;
	private String couName;
	private String couRemark;
	private Double couCredit;

	public Course() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cou_id", nullable = false, insertable = false, updatable = false)
	public Integer getCouId() {
		return this.couId;
	}

	public void setCouId(Integer couId) {
		this.couId = couId;
	}

	@Column(name = "cou_name", length = 100)
	public String getCouName() {
		return this.couName;
	}

	public void setCouName(String couName) {
		this.couName = couName;
	}

	@Column(name = "cou_remark", length = 200)
	public String getCouRemark() {
		return this.couRemark;
	}

	public void setCouRemark(String couRemark) {
		this.couRemark = couRemark;
	}

	@Column(name = "cou_credit")
	public Double getCouCredit() {
		return this.couCredit;
	}

	public void setCouCredit(Double couCredit) {
		this.couCredit = couCredit;
	}

}