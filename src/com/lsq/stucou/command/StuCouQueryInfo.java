package com.lsq.stucou.command;

import java.util.Date;

import com.lsq.common.command.BaseQueryInfo;

public class StuCouQueryInfo extends BaseQueryInfo{

	private static final long serialVersionUID = 3077763467776809153L;
	private Integer stuId;
	private Integer couId;
	private String couName;
	private String couRemark;
	private String couCredit;
	private Double scoreFront;
	private Double scoreBack;
	private String stuName;
	private String stuSex;
	private Integer stuAge;
	private Date stuBirthday;
	private Integer teaId;
	private String teaName;
	private String teaSex;
	private Integer teaAge;
	private Date teaBirthday;
	private Integer stuCouId;
	private Double score;
	private String teaCollege;
	private String stuClass;

	public Integer getStuId() {
		return stuId;
	}

	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}

	public Integer getCouId() {
		return couId;
	}

	public void setCouId(Integer couId) {
		this.couId = couId;
	}

	public String getCouName() {
		return couName;
	}

	public void setCouName(String couName) {
		this.couName = couName;
	}

	public String getCouRemark() {
		return couRemark;
	}

	public void setCouRemark(String couRemark) {
		this.couRemark = couRemark;
	}

	public String getCouCredit() {
		return couCredit;
	}

	public void setCouCredit(String couCredit) {
		this.couCredit = couCredit;
	}

	public Double getScoreFront() {
		return scoreFront;
	}

	public void setScoreFront(Double scoreFront) {
		this.scoreFront = scoreFront;
	}

	public Double getScoreBack() {
		return scoreBack;
	}

	public void setScoreBack(Double scoreBack) {
		this.scoreBack = scoreBack;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public String getStuSex() {
		return stuSex;
	}

	public void setStuSex(String stuSex) {
		this.stuSex = stuSex;
	}

	public Integer getStuAge() {
		return stuAge;
	}

	public void setStuAge(Integer stuAge) {
		this.stuAge = stuAge;
	}

	public Date getStuBirthday() {
		return stuBirthday;
	}

	public void setStuBirthday(Date stuBirthday) {
		this.stuBirthday = stuBirthday;
	}

	public Integer getTeaId() {
		return teaId;
	}

	public void setTeaId(Integer teaId) {
		this.teaId = teaId;
	}

	public String getTeaName() {
		return teaName;
	}

	public void setTeaName(String teaName) {
		this.teaName = teaName;
	}

	public String getTeaSex() {
		return teaSex;
	}

	public void setTeaSex(String teaSex) {
		this.teaSex = teaSex;
	}

	public Integer getTeaAge() {
		return teaAge;
	}

	public void setTeaAge(Integer teaAge) {
		this.teaAge = teaAge;
	}

	public Date getTeaBirthday() {
		return teaBirthday;
	}

	public void setTeaBirthday(Date teaBirthday) {
		this.teaBirthday = teaBirthday;
	}

	public Integer getStuCouId() {
		return stuCouId;
	}

	public void setStuCouId(Integer stuCouId) {
		this.stuCouId = stuCouId;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getTeaCollege() {
		return teaCollege;
	}

	public void setTeaCollege(String teaCollege) {
		this.teaCollege = teaCollege;
	}

	public String getStuClass() {
		return stuClass;
	}

	public void setStuClass(String stuClass) {
		this.stuClass = stuClass;
	}

}
