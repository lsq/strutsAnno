/**
 * IStuCouService.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.service;

import com.lsq.common.page.Page;
import com.lsq.stucou.command.StuCouQueryInfo;
import com.lsq.stucou.entity.StuCou;

/**
 * @author Lisq
 */
public interface IStuCouService {

	Page<StuCou> findStudentScorePage(StuCouQueryInfo info);

	Page<StuCou> findTeacherScorePage(StuCouQueryInfo info);

	StuCou findStuCou(StuCouQueryInfo info);

	void updateStuCou(StuCou stuCou);

}
