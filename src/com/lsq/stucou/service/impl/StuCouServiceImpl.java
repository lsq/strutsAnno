/**
 * StuCouServiceImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.stucou.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.common.page.Page;
import com.lsq.stucou.command.StuCouQueryInfo;
import com.lsq.stucou.dao.IStuCouDao;
import com.lsq.stucou.entity.StuCou;
import com.lsq.stucou.service.IStuCouService;

/**
 * @author Lisq
 */
@Service(value = "stuCouService")
public class StuCouServiceImpl implements IStuCouService{

	@Autowired
	private IStuCouDao stuCouDao;

	public Page<StuCou> findStudentScorePage(StuCouQueryInfo info) {
		return stuCouDao.findStudentScorePage(info);
	}

	@Override
	public Page<StuCou> findTeacherScorePage(StuCouQueryInfo info) {
		return stuCouDao.findTeacherScorePage(info);
	}

	@Override
	public StuCou findStuCou(StuCouQueryInfo info) {
		return stuCouDao.findStuCou(info);
	}

	@Override
	public void updateStuCou(StuCou stuCou) {
		stuCouDao.updateStuCou(stuCou);
	}

}
