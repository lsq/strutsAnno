/**
 * ManageDaoImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.manager.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.lsq.common.command.BaseQueryInfo;
import com.lsq.common.dao.impl.BaseDaoImpl;
import com.lsq.common.page.Page;
import com.lsq.common.util.Tools;
import com.lsq.manager.command.ManageQueryInfo;
import com.lsq.manager.dao.IManageDao;
import com.lsq.stucou.entity.Course;
import com.lsq.stucou.entity.StuCou;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 * @param <T>
 */
@Repository(value = "manageDao")
public class ManageDaoImpl<T> extends BaseDaoImpl<T, Integer> implements
		IManageDao<T> {

	private Page<T> getMyPage(BaseQueryInfo info, String hql) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<T> data = this.executeSql(hql);
			return this.putDataToPage(data);
		} else {
			return this.find(hql, info.getPageNumber().intValue(), info
					.getPageSize().intValue());
		}
	}

	private Page<T> getMyPage(BaseQueryInfo info, String hql, Object[] condition) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<T> data = this.executeSql(hql, condition);
			return this.putDataToPage(data);
		} else {
			return this.find(hql, condition, info.getPageNumber().intValue(),
					info.getPageSize().intValue());
		}
	}

	@Override
	public Page<T> findStudentPage(ManageQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Student.class.getName())
				.append(" as s where 1=1 ");

		if (Tools.isNotEmptyString(info.getStuName())) {
			hql.append(" and s.stuName like ? ");
			values[idx++] = "%" + info.getStuName() + "%";
		}

		hql.append(" order by s.stuId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return this.getMyPage(info, hql.toString(), condition);
		}
		return this.getMyPage(info, hql.toString());
	}

	@Override
	public void addStudent(T student) {
		this.save(student);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T toUpdateStudent(ManageQueryInfo info) {
		return (T) this.load((Class<T>) Student.class, info.getStuId());
	}

	@Override
	public void updateStudent(T student) {
		this.addStudent(student);
	}

	@Override
	public Page<T> findTeacherPage(ManageQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Teacher.class.getName())
				.append(" as t where 1=1 ");

		if (Tools.isNotEmptyString(info.getTeaName())) {
			hql.append(" and t.teaName like ? ");
			values[idx++] = "%" + info.getTeaName() + "%";
		}

		hql.append(" order by t.teaId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<T>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<T>) this.getMyPage(info, hql.toString());
	}

	@Override
	public void addTeacher(T teacher) {
		this.save(teacher);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T toUpdateTeacher(ManageQueryInfo info) {
		return this.load((Class<T>) Teacher.class, info.getTeaId());
	}

	@Override
	public void updateTeacher(T teacher) {
		this.addTeacher(teacher);
	}

	@Override
	public Page<T> findCoursePage(ManageQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Course.class.getName())
				.append(" as c where 1=1 ");

		if (Tools.isNotEmptyString(info.getCouName())) {
			hql.append(" and c.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}

		hql.append(" order by c.couId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<T>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<T>) this.getMyPage(info, hql.toString());
	}

	@Override
	public void addCourse(T course) {
		this.save(course);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T toUpdateCourse(ManageQueryInfo info) {
		return this.load((Class<T>) Course.class, info.getCouId());
	}

	@Override
	public void updateCourse(T course) {
		this.addCourse(course);
	}

	@Override
	public Page<T> findStuCouPage(ManageQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(StuCou.class.getName())
				.append(" as sc where 1=1 ");

		if (info.getStuId() != null) {
			hql.append(" and sc.student.stuId = ? ");
			values[idx++] = info.getStuId();
		}

		if (info.getTeaId() != null) {
			hql.append(" and sc.teacher.teaId = ? ");
			values[idx++] = info.getTeaId();
		}

		if (info.getTeaId() != null) {
			hql.append(" and sc.course.couId = ? ");
			values[idx++] = info.getTeaId();
		}

		if (Tools.isNotEmptyString(info.getCouName())) {
			hql.append(" and sc.course.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}

		if (Tools.isNotEmptyString(info.getTeaName())) {
			hql.append(" and sc.teacher.teaName like ? ");
			values[idx++] = "%" + info.getTeaName() + "%";
		}

		if (Tools.isNotEmptyString(info.getStuName())) {
			hql.append(" and sc.student.stuName like ? ");
			values[idx++] = "%" + info.getStuName() + "%";
		}

		if (info.getScoreFront() != null) {
			hql.append(" and sc.score >= ? ");
			values[idx++] = info.getScoreFront();
		}

		if (info.getScoreBack() != null) {
			hql.append(" and sc.score <= ? ");
			values[idx++] = info.getScoreBack();
		}

		hql.append(" order by sc.course.couId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<T>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<T>) this.getMyPage(info, hql.toString());
	}

	@Override
	public void addStuCou(T stuCou) {
		this.save(stuCou);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T toUpdateStuCou(ManageQueryInfo info) {
		return this.load((Class<T>) StuCou.class, info.getStuCouId());
	}

	@Override
	public void updateStuCou(T stuCou) {
		this.addStuCou(stuCou);
	}

	@Override
	public void deleteStuCou(T delStuCou) {
		this.delete(delStuCou);
	}

}
