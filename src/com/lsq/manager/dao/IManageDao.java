/**
 * IManageDao.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.manager.dao;

import com.lsq.common.dao.IBaseDao;
import com.lsq.common.page.Page;
import com.lsq.manager.command.ManageQueryInfo;

public interface IManageDao<T> extends IBaseDao<T, Integer> {

	Page<T> findStudentPage(ManageQueryInfo info);

	void addStudent(T student);

	T toUpdateStudent(ManageQueryInfo info);

	void updateStudent(T student);

	Page<T> findTeacherPage(ManageQueryInfo info);

	void addTeacher(T teacher);

	T toUpdateTeacher(ManageQueryInfo info);

	void updateTeacher(T teacher);

	Page<T> findCoursePage(ManageQueryInfo info);

	void addCourse(T course);

	T toUpdateCourse(ManageQueryInfo info);

	void updateCourse(T course);

	Page<T> findStuCouPage(ManageQueryInfo info);

	void addStuCou(T stuCou);

	T toUpdateStuCou(ManageQueryInfo info);

	void updateStuCou(T stuCou);

	void deleteStuCou(T delStuCou);

}
