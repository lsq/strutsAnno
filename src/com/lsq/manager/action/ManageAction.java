/**
 * ManageAction.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.manager.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.lsq.common.action.BaseAction;
import com.lsq.common.page.Page;
import com.lsq.manager.command.ManageQueryInfo;
import com.lsq.manager.service.IManageService;
import com.lsq.stucou.entity.Course;
import com.lsq.stucou.entity.StuCou;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */
@Namespace("/manage")
@Controller("manageAction")
@Scope(value = "prototype")
public class ManageAction extends BaseAction {

	private static final long serialVersionUID = -4436850933436607106L;

	private Page<Student> students;
	private Page<Teacher> teachers;
	private Page<Course> courses;
	private Page<StuCou> stuCous;
	private Student student = new Student();
	private Teacher teacher = new Teacher();
	private Course course = new Course();
	private StuCou stuCou = new StuCou();
	private String adminUserName;
	private ManageQueryInfo info = new ManageQueryInfo();

	@Autowired
	private IManageService<Student> studentService;

	@Autowired
	private IManageService<Teacher> teacherService;

	@Autowired
	private IManageService<Course> courseService;

	@Autowired
	private IManageService<StuCou> stuCouService;

	@Action(value = "navigation", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/navigation.vm") })
	public String navigation() {
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	@Action(value = "findStudentPage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/findStudentPage.vm") })
	public String findStudentPage() {
		students = studentService.findStudentPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	@Action(value = "toAddStudent", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/student.vm") })
	public String toAddStudent() {
		return SUCCESS;
	}

	@Action(value = "addStudent", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findStudentPage") })
	public String addStudent() {
		studentService.addStudent(student);
		return SUCCESS;
	}

	@Action(value = "toUpdateStudent", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/student.vm") })
	public String toUpdateStudent() {
		student = studentService.toUpdateStudent(info);
		return SUCCESS;
	}

	@Action(value = "updateStudent", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findStudentPage") })
	public String updateStudent() {
		studentService.updateStudent(student);
		return SUCCESS;
	}

	@Action(value = "findTeacherPage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/findTeacherPage.vm") })
	public String findTeacherPage() {
		teachers = teacherService.findTeacherPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	@Action(value = "toAddTeacher", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/teacher.vm") })
	public String toAddTeacher() {
		return SUCCESS;
	}

	@Action(value = "addTeacher", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findTeacherPage") })
	public String addTeacher() {
		teacherService.addTeacher(teacher);
		return SUCCESS;
	}

	@Action(value = "toUpdateTeacher", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/teacher.vm") })
	public String toUpdateTeacher() {
		teacher = teacherService.toUpdateTeacher(info);
		return SUCCESS;
	}

	@Action(value = "updateTeacher", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findTeacherPage") })
	public String updateTeacher() {
		teacherService.updateTeacher(teacher);
		return SUCCESS;
	}

	@Action(value = "findCoursePage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/findCoursePage.vm") })
	public String findCoursePage() {
		courses = courseService.findCoursePage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	@Action(value = "toAddCourse", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/course.vm") })
	public String toAddCourse() {
		return SUCCESS;
	}

	@Action(value = "addCourse", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findCoursePage") })
	public String addCourse() {
		courseService.addCourse(course);
		return SUCCESS;
	}

	@Action(value = "toUpdateCourse", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/course.vm") })
	public String toUpdateCourse() {
		course = courseService.toUpdateCourse(info);
		return SUCCESS;
	}

	@Action(value = "updateCourse", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findCoursePage") })
	public String updateCourse() {
		courseService.updateCourse(course);
		return SUCCESS;
	}

	@Action(value = "findStuCouPage", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/findStuCouPage.vm") })
	public String findStuCouPage() {
		stuCous = stuCouService.findStuCouPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	@Action(value = "toAddStuCou", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/stuCou.vm") })
	public String toAddStuCou() {
		info.setNotPage(true);
		students = studentService.findStudentPage(info);
		teachers = teacherService.findTeacherPage(info);
		courses = courseService.findCoursePage(info);
		return SUCCESS;
	}

	@Action(value = "addStuCou", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findStuCouPage") })
	public String addStuCou() {
		stuCouService.addStuCou(stuCou);
		return SUCCESS;
	}

	@Action(value = "deleteStuCou", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findStuCouPage") })
	public String deleteStuCou() {
		StuCou delStuCou = new StuCou();
		delStuCou.setStuCouId(info.getStuCouId());
		stuCouService.deleteStuCou(delStuCou);
		return SUCCESS;
	}

	@Action(value = "toUpdateStuCou", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/admin/stuCou.vm") })
	public String toUpdateStuCou() {
		stuCou = stuCouService.toUpdateStuCou(info);
		students = studentService.findStudentPage(info);
		teachers = teacherService.findTeacherPage(info);
		courses = courseService.findCoursePage(info);
		return SUCCESS;
	}

	@Action(value = "updateStuCou", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findStuCouPage") })
	public String updateStuCou() {
		stuCouService.updateStuCou(stuCou);
		return SUCCESS;
	}

	public Page<Student> getStudents() {
		return students;
	}

	public void setStudents(Page<Student> students) {
		this.students = students;
	}

	public Page<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Page<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Page<Course> getCourses() {
		return courses;
	}

	public void setCourses(Page<Course> courses) {
		this.courses = courses;
	}

	public Page<StuCou> getStuCous() {
		return stuCous;
	}

	public void setStuCous(Page<StuCou> stuCous) {
		this.stuCous = stuCous;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public StuCou getStuCou() {
		return stuCou;
	}

	public void setStuCou(StuCou stuCou) {
		this.stuCou = stuCou;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public ManageQueryInfo getInfo() {
		return info;
	}

	public void setInfo(ManageQueryInfo info) {
		this.info = info;
	}

}
