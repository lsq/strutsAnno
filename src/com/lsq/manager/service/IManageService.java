/**
 * IManageService.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.manager.service;

import com.lsq.common.page.Page;
import com.lsq.manager.command.ManageQueryInfo;

/**
 * @author Lisq
 */
public interface IManageService<T> {

	Page<T> findStudentPage(ManageQueryInfo info);

	void addStudent(T student);

	T toUpdateStudent(ManageQueryInfo info);

	void updateStudent(T student);

	Page<T> findTeacherPage(ManageQueryInfo info);

	void addTeacher(T teacher);

	T toUpdateTeacher(ManageQueryInfo info);

	void updateTeacher(T teacher);

	Page<T> findCoursePage(ManageQueryInfo info);

	void addCourse(T course);

	T toUpdateCourse(ManageQueryInfo info);

	void updateCourse(T course);

	Page<T> findStuCouPage(ManageQueryInfo info);

	void addStuCou(T stuCou);

	void deleteStuCou(T delStuCou);

	T toUpdateStuCou(ManageQueryInfo info);

	void updateStuCou(T stuCou);

}
