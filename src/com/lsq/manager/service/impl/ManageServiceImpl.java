/**
 * ManageServiceImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.manager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.common.page.Page;
import com.lsq.manager.command.ManageQueryInfo;
import com.lsq.manager.dao.IManageDao;
import com.lsq.manager.service.IManageService;

/**
 * @author Lisq
 * @param <T>
 */
@Service(value = "manageService")
public class ManageServiceImpl<T> implements IManageService<T> {

	@Autowired
	private IManageDao<T> manageDao;

	@Override
	public Page<T> findStudentPage(ManageQueryInfo info) {
		return manageDao.findStudentPage(info);
	}

	@Override
	public void addStudent(T student) {
		manageDao.addStudent(student);
	}

	@Override
	public T toUpdateStudent(ManageQueryInfo info) {
		return manageDao.toUpdateStudent(info);
	}

	@Override
	public void updateStudent(T student) {
		manageDao.updateStudent(student);
	}

	@Override
	public Page<T> findTeacherPage(ManageQueryInfo info) {
		return manageDao.findTeacherPage(info);
	}

	@Override
	public void addTeacher(T teacher) {
		manageDao.addTeacher(teacher);
	}

	@Override
	public T toUpdateTeacher(ManageQueryInfo info) {
		return manageDao.toUpdateTeacher(info);
	}

	@Override
	public void updateTeacher(T teacher) {
		manageDao.updateTeacher(teacher);
	}

	@Override
	public Page<T> findCoursePage(ManageQueryInfo info) {
		return manageDao.findCoursePage(info);
	}

	@Override
	public void addCourse(T course) {
		manageDao.addCourse(course);
	}

	@Override
	public T toUpdateCourse(ManageQueryInfo info) {
		return manageDao.toUpdateCourse(info);
	}

	@Override
	public void updateCourse(T course) {
		manageDao.updateCourse(course);
	}

	@Override
	public Page<T> findStuCouPage(ManageQueryInfo info) {
		return manageDao.findStuCouPage(info);
	}

	@Override
	public void addStuCou(T stuCou) {
		manageDao.addStuCou(stuCou);
	}

	@Override
	public void deleteStuCou(T delStuCou) {
		manageDao.deleteStuCou(delStuCou);
	}

	@Override
	public T toUpdateStuCou(ManageQueryInfo info) {
		return manageDao.toUpdateStuCou(info);
	}

	@Override
	public void updateStuCou(T stuCou) {
		manageDao.updateStuCou(stuCou);
	}

}
