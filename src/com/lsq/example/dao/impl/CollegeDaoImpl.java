/**
 * CollegeDaoImpl.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.lsq.common.command.BaseQueryInfo;
import com.lsq.common.dao.impl.BaseDaoImpl;
import com.lsq.common.page.Page;
import com.lsq.example.command.CollegeQueryInfo;
import com.lsq.example.dao.ICollegeDao;
import com.lsq.example.entity.College;

/**
 * @author Lisq
 */
@Repository(value = "collegeDao")
public class CollegeDaoImpl extends BaseDaoImpl<College, Integer> implements
		ICollegeDao {

	private Page<College> getMyPage(BaseQueryInfo info, String hql) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<College> data = this.executeSql(hql);
			return this.putDataToPage(data);
		} else {
			return super.find(hql, info.getPageNumber().intValue(), info
					.getPageSize().intValue());
		}
	}

	private Page<College> getMyPage(BaseQueryInfo info, String hql,
			Object[] condition) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<College> data = this.executeSql(hql, condition);
			return this.putDataToPage(data);
		} else {
			return super.find(hql, condition, info.getPageNumber().intValue(),
					info.getPageSize().intValue());
		}
	}

	public Page<College> findCollegePage(CollegeQueryInfo info) {
		Object[] values = new Object[50];
		int idx = 0;

		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(College.class.getName())
				.append(" as c where 1=1 ");

		// if(info.getStuId() != null){
		// hql.append(" and sc.stuId = ? ");
		// values[idx++] = info.getStuId() ;
		// }

		// hql.append(" order by c.collegeId asc");

		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return this.getMyPage(info, hql.toString(), condition);
		}
		return this.getMyPage(info, hql.toString());
	}

}
