/**
 * ICollegeDao.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.dao;

import com.lsq.common.dao.IBaseDao;
import com.lsq.common.page.Page;
import com.lsq.example.command.CollegeQueryInfo;
import com.lsq.example.entity.College;

/**
 * @author Lisq
 */
public interface ICollegeDao extends IBaseDao<College, Integer> {
	Page<College> findCollegePage(CollegeQueryInfo info);
}
