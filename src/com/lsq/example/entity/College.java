/**
 * College.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lsq.common.entity.AllEntity;

/**
 * @author Lisq
 */
@Entity(name = "College")
@Table(name = "college")
public class College extends AllEntity<College> {

	private static final long serialVersionUID = -209997691232213464L;

	private Integer collegeId;
	private String collegeName;

	public College() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "college_id", nullable = false, insertable = false, updatable = false)
	public Integer getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}

	@Column(name = "college_name", length = 50)
	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

}
