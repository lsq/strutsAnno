/**
 * CollegeAction.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.lsq.common.action.BaseAction;
import com.lsq.common.page.Page;
import com.lsq.example.command.CollegeQueryInfo;
import com.lsq.example.entity.College;
import com.lsq.example.service.ICollegeService;

/**
 * @author Lisq
 */
@Namespace("/college")
@Controller("collegeAction")
@Scope(value="prototype")
public class CollegeAction extends BaseAction {

	private static final long serialVersionUID = -5163000407787051834L;

	@Autowired
	private ICollegeService collegeService;

	private Page<College> colleges;
	private CollegeQueryInfo info = new CollegeQueryInfo();

	@Action(value = "findCollegePage", results = { @Result(location = "/WEB-INF/test/test.jsp") })
	public String findCollegePage() {
		colleges = collegeService.findCollegePage(info);
		return SUCCESS;
	}

	public Page<College> getColleges() {
		return colleges;
	}

	public void setColleges(Page<College> colleges) {
		this.colleges = colleges;
	}

	public CollegeQueryInfo getInfo() {
		return info;
	}

	public void setInfo(CollegeQueryInfo info) {
		this.info = info;
	}

	
}