/**
 * ICollegeService.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.service;

import com.lsq.common.page.Page;
import com.lsq.example.command.CollegeQueryInfo;
import com.lsq.example.entity.College;

/**
 * @author Lisq
 */
public interface ICollegeService {

	Page<College> findCollegePage(CollegeQueryInfo info);
}
