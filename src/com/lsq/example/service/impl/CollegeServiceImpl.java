/**
 * CollegeServiceImpl.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.example.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.common.page.Page;
import com.lsq.example.command.CollegeQueryInfo;
import com.lsq.example.dao.ICollegeDao;
import com.lsq.example.entity.College;
import com.lsq.example.service.ICollegeService;

/**
 * @author Lisq
 */
@Service(value="collegeService") 
public class CollegeServiceImpl implements ICollegeService {

	@Autowired
	private ICollegeDao collegeDao;

	public Page<College> findCollegePage(CollegeQueryInfo info) {
		return collegeDao.findCollegePage(info);
	}

}
