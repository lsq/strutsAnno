/**
 * TeacherDaoImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.dao.impl;

import org.springframework.stereotype.Repository;

import com.lsq.common.dao.impl.BaseDaoImpl;
import com.lsq.teacher.command.TeacherQueryInfo;
import com.lsq.teacher.dao.ITeacherDao;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */
@Repository(value = "teacherDao")
public class TeacherDaoImpl extends BaseDaoImpl<Teacher, Integer> implements
		ITeacherDao {

	public void updateTeacher(Teacher teacher) {
		this.save(teacher);
	}

	public Teacher findTeacher(TeacherQueryInfo info) {
		return this.load(Teacher.class, info.getTeaId());
	}

}
