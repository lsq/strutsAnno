/**
 * ITeacherDao.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.dao;

import com.lsq.common.dao.IBaseDao;
import com.lsq.teacher.command.TeacherQueryInfo;
import com.lsq.teacher.entity.Teacher;

public interface ITeacherDao extends IBaseDao<Teacher, Integer> {

	void updateTeacher(Teacher teacher);

	Teacher findTeacher(TeacherQueryInfo info);

}