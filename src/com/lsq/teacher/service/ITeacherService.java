/**
 * ITeacherService.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.service;

import com.lsq.teacher.command.TeacherQueryInfo;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */
public interface ITeacherService {

	void updateTeacher(Teacher teacher);

	Teacher findTeacher(TeacherQueryInfo info);

}
