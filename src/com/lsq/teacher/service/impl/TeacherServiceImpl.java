/**
 * TeacherServiceImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.teacher.command.TeacherQueryInfo;
import com.lsq.teacher.dao.ITeacherDao;
import com.lsq.teacher.entity.Teacher;
import com.lsq.teacher.service.ITeacherService;

/**
 * @author Lisq
 */
@Service(value = "teacherService")
public class TeacherServiceImpl implements ITeacherService{

	@Autowired
	private ITeacherDao teacherDao;

	public void updateTeacher(Teacher teacher) {
		teacherDao.updateTeacher(teacher);
	}

	public Teacher findTeacher(TeacherQueryInfo info) {
		return teacherDao.findTeacher(info);
	}

}
