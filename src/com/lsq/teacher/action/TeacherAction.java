/**
 * TeacherAction.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.lsq.common.action.BaseAction;
import com.lsq.teacher.command.TeacherQueryInfo;
import com.lsq.teacher.entity.Teacher;
import com.lsq.teacher.service.ITeacherService;

/**
 * @author Lisq
 */
@Namespace("/stusys")
@Controller("teacherAction")
@Scope(value = "prototype")
public class TeacherAction extends BaseAction {

	private static final long serialVersionUID = -4436850933436607106L;

	private Teacher teacher = new Teacher();
	private TeacherQueryInfo info = new TeacherQueryInfo();

	@Autowired
	private ITeacherService teacherService;

	@Action(value = "toUpdateTeacher", results = { @Result(name = SUCCESS, type = "velocity", location = "/WEB-INF/teacher/updateTeacher.vm") })
	public String toUpdateTeacher() {
		teacher = teacherService.findTeacher(info);
		return SUCCESS;
	}

	@Action(value = "updateTeacher", results = { @Result(name = SUCCESS, type = "redirectAction", location = "findTeacherScorePage") })
	public String updateTeacher() {
		teacherService.updateTeacher(teacher);
		return SUCCESS;
	}
	
	/**
	 * @return the teacher
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * @param teacher
	 *            the teacher to set
	 */
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	/**
	 * @return the info
	 */
	public TeacherQueryInfo getInfo() {
		return info;
	}

	/**
	 * @param info
	 *            the info to set
	 */
	public void setInfo(TeacherQueryInfo info) {
		this.info = info;
	}

}
