/**
 * Teacher.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.teacher.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lsq.common.entity.AllEntity;
import com.lsq.common.util.DateUtil;

/**
 * @author Lisq
 */
@Entity(name = "Teacher")
@Table(name = "teacher")
public class Teacher extends AllEntity<Teacher>{

	private static final long serialVersionUID = -1456650035353513250L;
	
	private Integer teaId;
	private String teaName;
	private String teaSex;
	private Integer teaAge;
	private Date teaBirthday;
	private String teaCollege;
	private String userName;
	private String password;

	public Teacher() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "tea_id", nullable = false, insertable = false, updatable = false)
	public Integer getTeaId() {
		return this.teaId;
	}

	public void setTeaId(Integer teaId) {
		this.teaId = teaId;
	}

	@Column(name = "tea_name", length = 50)
	public String getTeaName() {
		return this.teaName;
	}

	public void setTeaName(String teaName) {
		this.teaName = teaName;
	}

	@Column(name = "tea_sex", length = 1)
	public String getTeaSex() {
		return this.teaSex;
	}

	public void setTeaSex(String teaSex) {
		this.teaSex = teaSex;
	}

	@Column(name = "tea_age")
	public Integer getTeaAge() {
		return this.teaAge;
	}

	public void setTeaAge(Integer teaAge) {
		this.teaAge = teaAge;
	}

	@Column(name = "tea_birthday")
	@Temporal(TemporalType.DATE)
	public Date getTeaBirthday() {
		return this.teaBirthday;
	}

	public void setTeaBirthday(Date teaBirthday) {
		this.teaBirthday = teaBirthday;
	}

	@Column(name = "tea_college", length = 50)
	public String getTeaCollege() {
		return teaCollege;
	}

	public void setTeaCollege(String teaCollege) {
		this.teaCollege = teaCollege;
	}

	@Column(name = "user_name", length = 20)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "password", length = 20)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String formatDate() {
		if (this.teaBirthday != null) {
			return DateUtil.DATE_FORMAT_YMD.format(this.teaBirthday).toString();
		} else {
			return null;
		}
	}
}