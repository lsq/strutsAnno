package com.lsq.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

	/** 按 yyyy-MM-dd HH:mm:ss 格式化时间. */
	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	/** 按 yyyy-MM-dd 格式化日期. */
	public static final SimpleDateFormat DATE_FORMAT_YMD = new SimpleDateFormat(
			"yyyy-MM-dd");

	/** 按 yyyy-MM-dd HH:mm 格式化时间. */
	public static final SimpleDateFormat DATE_MINUTE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");

	public static final SimpleDateFormat DATE_FORMAT_YM = new SimpleDateFormat(
			"yyyy-MM");

	public static final SimpleDateFormat DATE_FORMAT_YM_CN = new SimpleDateFormat(
			"yyyy年MM月");

	/** 按 yyyy/MM/dd 格式化日期. */
	public static final SimpleDateFormat DATE_FORMAT_YMD2 = new SimpleDateFormat(
			"yyyy/MM/dd");

	/** 按 yyyy年MM月dd日 格式化日期. */
	public static final SimpleDateFormat DATE_FORMAT_YMD_CH = new SimpleDateFormat(
			"yyyy年MM月dd日");

	public static String formatTime(Date date) {
		return date == null ? "" : DateUtil.DATE_TIME_FORMAT.format(date);
	}

	public static String dateFormatYMD(Date date) {
		return date == null ? "" : DATE_FORMAT_YMD.format(date);
	}

	public static String dateTimeFormat(Date date) {
		return date == null ? "" : DATE_TIME_FORMAT.format(date);
	}

	public static String dateFormatYY_MM(Date date) {
		return date == null ? "" : DATE_FORMAT_YM.format(date);
	}

	public static String dateFormatYM_CH(Date date) {
		return date == null ? "" : DATE_FORMAT_YM_CN.format(date);
	}

	public static String dateFormatYMD2(Date date) {
		return date == null ? "" : DATE_FORMAT_YMD2.format(date);
	}

	public static String dateFormatYMD_CH(Date date) {
		return date == null ? "" : DATE_FORMAT_YMD_CH.format(date);
	}

	public static Date convertStringToDate(String dateString) {
		String[] dateStrArray = dateString.split("-");
		return new GregorianCalendar(Integer.parseInt(dateStrArray[0]), Integer
				.parseInt(dateStrArray[1]) - 1, Integer
				.parseInt(dateStrArray[2])).getTime();
	}

	public static String chineseWeekDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
		String[] weeks = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		return weeks[weekDay - 1];
	}

}
