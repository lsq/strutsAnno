﻿/**
 * HelperObjectCache.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.util;

import java.util.Map;

/**
 * @author Lisq
 */
public final class HelperObjectCache {

    private static Map<String, ?> helperMap = null;

    private static boolean allowHelperOverride = false;

    /**
     * @return Returns the helperMap.
     */
    public static Map<String, ?> getHelperMap() {
        return helperMap;
    }

    /**
     * @param helperMap The helperMap to set.
     */
    public void setHelperMap(Map<String, ?> helperMap) {
        HelperObjectCache.helperMap = helperMap;
    }

    /**
     * @return  是否允许覆盖同名上下文
     */
    public static boolean getAllowHelperOverride() {
        return allowHelperOverride;
    }

    /**
     * @param allowHelperOverride The allowHelperOverride to set.
     */
    public void setAllowHelperOverride(boolean allowHelperOverride) {
        HelperObjectCache.allowHelperOverride = allowHelperOverride;
    }
}

