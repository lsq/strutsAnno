package com.lsq.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Tools {
	
	/** 数值格式化 */
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.######");

	/**
	 * 数字大写的中文字符
	 */
	private static final String[] CHINESECODE = { "零", "壹", "贰", "叁", "肆", "伍",
			"陆", "柒", "捌", "玖", "分", "角", "元", "拾", "佰", "仟", "万", "拾万", "佰万",
			"仟万", "亿", "拾亿", "百亿", "仟亿" };

	/** 唯一实例 */
	private static final Tools INSTANCE = new Tools();

	public Tools() {
	}

	/**
	 * 取得实例
	 * @return Tools
	 */
	public static Tools getInstance() {
		return INSTANCE;
	}

	/**
	 * 格式化BigDecimal为字符串
	 * @param 输入的BigDecimal
	 * @return 格式化的字符串
	 */
	public static String bigDecimalToString(BigDecimal value) {
		String returnVal = value.toString();
		if (returnVal.length() - returnVal.indexOf(".") > 4) {
			returnVal = returnVal.substring(0, returnVal.indexOf(".") + 5);
		} else if (returnVal.indexOf(".") == -1) {
			returnVal = new StringBuffer(returnVal).append(".00").toString();
		}
		return returnVal;
	}

	/**
	 * @param 数字金额charge
	 * @return String 大写中文金额
	 */
	public static String numericToChinese(String charge) {
		String tempCharge;
		String chargeChinese = "";

		if (charge.indexOf(".") > 0) {
			tempCharge = charge.substring(0, charge.indexOf("."));
			tempCharge += charge.substring(charge.indexOf(".") + 1, charge
					.length());
		} else {
			tempCharge = charge + "00";
		}
		for (int i = 0; i < tempCharge.length(); i++) {
			chargeChinese += CHINESECODE[(Integer.parseInt(tempCharge
					.substring(i, i + 1)) + 1) - 1]
					+ CHINESECODE[(10 + tempCharge.length() - i) - 1];
		}
		return chargeChinese + "整";
	}

	/**
	 * 判断字符串是否为null或空串
	 */
	public static boolean isNotEmptyString(String str) {
		return !(str == null || str.trim().length() == 0);
	}

	/**
	 * 判断list是否为非空列表（null和零长度的字符串都是空）
	 * @param <T>
	 * @param 源列表list
	 * @return true：非空列表 false：空列表
	 */
	public static <T> boolean isNotEmptyList(List<T> list) {
		return (list != null && !list.isEmpty());
	}

	/**
	 * 判断字符串是否为非空Map（null和零长度的字符串都是空）
	 * @param 源map
	 * @return true：非空map false：空map
	 */
	public static boolean isNotEmptyMap(Map<Object, Object> map) {
		return (map != null && !map.isEmpty());
	}

	/**
	 * 将null对象转化为空串""
	 * @param str
	 * @return
	 */
	public static String nullToSpaceStr(Object str) {
		if (str == null) {
			return "";
		} else {
			return str.toString().trim();
		}
	}

	/**
	 * 判断是否为整数
	 */
	public static boolean isNumStr(String s) {
		if (s == null)
			return false;
		return s.matches("[0-9]+");
	}

	/**
	 * 获得一个15位的唯一数值型ID
	 * @return
	 */
	public static Long getIdentityNumber15() {
		SimpleDateFormat DATE_RAMDOM_STR = new SimpleDateFormat("yyMMddHHmmss");
		NumberFormat nf3 = new DecimalFormat("000");
		String s = DATE_RAMDOM_STR.format(new Date())
				+ nf3.format((Math.random() * 999));
		return Long.parseLong(s);
	}
}
