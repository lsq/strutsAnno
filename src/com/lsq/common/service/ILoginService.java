/**
 * ILoginService.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.service;

import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 */

public interface ILoginService {

	Student validateStudent(String userName, String password);

	Teacher validateTeacher(String userName, String password);

}
