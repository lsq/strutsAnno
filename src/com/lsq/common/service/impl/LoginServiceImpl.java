/**
 * LoginServiceImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lsq.common.dao.ILoginDao;
import com.lsq.common.service.ILoginService;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 * @param <T>
 */
@Service(value="loginService") 
public class LoginServiceImpl implements ILoginService{

	@Autowired
	private ILoginDao loginDao;

	public Student validateStudent(String userName, String password) {
		return loginDao.validateStudent(userName, password);
	}

	public Teacher validateTeacher(String userName, String password) {
		return loginDao.validateTeacher(userName, password);
	}
}
