/**
 * MainAction.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.stereotype.Controller;

/**
 * @author Lisq
 */
@Namespace("/")
@Controller("mainAction")
public class MainAction extends BaseAction {

	private static final long serialVersionUID = 7687125123600020450L;

	@Action(value = "main", results = {
			@Result(name = SUCCESS, location = "/login.html")})
	public String main() {
		return SUCCESS;
	}
}
