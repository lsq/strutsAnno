/**
 * IBaseDao.java
 * create by Lisq
 * date 2014-1-6
 */
package com.lsq.common.dao;

import java.io.Serializable;
import java.util.List;

import com.lsq.common.page.Page;

/**
 * @author Lisq
 */
public interface IBaseDao<T, ID extends Serializable> {

	/**
	 * 新增或更新一个对象
	 * 
	 * @param entity
	 *            需保存的对象
	 */
	void save(final T entity);

	/**
	 * 删除一个对象
	 * 
	 * @param entity
	 *            需删除的对象
	 */
	void delete(final T entity);

	/**
	 * 取一个持久对象
	 * 
	 * @param entityClass
	 *            持久类
	 * @param id
	 *            标识
	 * @return 持久对象
	 */
	T load(final Class<T> entityClass, final Serializable id);

	/**
	 * 获取一个类的所有实例
	 * 
	 * @param entityClass
	 *            类型
	 * @return 对象列表
	 */
	List<T> loadAll(final Class<T> entityClass);

	/**
	 * 执行查询语句,返回结果列表
	 * 
	 * @param queryString
	 *            查询语句
	 * @return 结果列表
	 */
	List<T> executeSql(String queryString);

	/**
	 * 执行查询语句,返回结果列表
	 * 
	 * @param queryString
	 *            查询语句
	 * @param values
	 *            参数值
	 * @return 结果列表
	 */
	List<T> executeSql(String queryString, final Object[] values);

	// ////////分页查询
	/**
	 * @param queryString
	 *            HSQL
	 * @param pageNumber
	 *            当前页码
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final int pageNumber);

	/**
	 * @param queryString
	 *            HSQL
	 * @param pageNumber
	 *            当前页码
	 * @param pageSize
	 *            每页显示的记录数
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final int pageNumber,
			final int pageSize);

	/**
	 * @param queryString
	 *            HSQL
	 * @param value
	 *            参数值
	 * @param pageNumber
	 *            当前页码
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final Object value,
			final int pageNumber);

	/**
	 * @param queryString
	 *            HSQL
	 * @param value
	 *            参数值
	 * @param pageNumber
	 *            当前页码
	 * @param pageSize
	 *            每页显示的记录数
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final Object value,
			final int pageNumber, final int pageSize);

	/**
	 * @param queryString
	 *            HSQL
	 * @param values
	 *            参数值
	 * @param pageNumber
	 *            当前页码
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final Object[] values,
			final int pageNumber);

	/**
	 * @param queryString
	 *            HSQL
	 * @param values
	 *            参数值
	 * @param pageNumber
	 *            当前页码
	 * @param pageSize
	 *            每页显示的记录数
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final Object[] values,
			final int pageNumber, final int pageSize);

	/**
	 * @param queryString
	 *            HSQL
	 * @param fieldString
	 *            显示字段
	 * @param values
	 *            参数值
	 * @param pageNumber
	 *            当前页码
	 * @return Page实例
	 */
	Page<T> find(final String queryString, final String fieldString,
			final Object[] values, final int pageNumber);

	/**
	 * 建立一个空的Page，并将data放到page的data中。
	 * 
	 * @param data
	 *            数据
	 * @return page
	 */
	Page<T> putDataToPage(List<T> data);

	/**
	 * 建立一个空的Page，并将data放到page的data中。
	 * 
	 * @param data
	 * @param pageNumber
	 * @return
	 */
	Page<T> putDataToPage(List<T> data, final int pageNumber);
}
