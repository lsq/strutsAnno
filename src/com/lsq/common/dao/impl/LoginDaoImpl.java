/**
 * LoginDaoImpl.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.lsq.common.dao.ILoginDao;
import com.lsq.common.util.Tools;
import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

/**
 * @author Lisq
 * @param <T>
 */
@Repository(value = "loginDao")
public class LoginDaoImpl extends BaseDaoImpl<Object, Integer> implements ILoginDao{

	public Student validateStudent(String userName, String password) {
		StringBuffer hsql = new StringBuffer();
		hsql.append("from ").append(Student.class.getName()).append(" as s where s.userName = ? and s.password = ?");
		List<Object> students =  this.executeSql(hsql.toString(), new Object[]{userName, password});
		if(Tools.isNotEmptyList(students)){
			return (Student) students.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Teacher validateTeacher(String userName, String password) {
		StringBuffer hsql = new StringBuffer();
		hsql.append("from ").append(Teacher.class.getName()).append(" as t where t.userName = ? and t.password = ?");
		List<Object> teachers =  this.executeSql(hsql.toString(), new Object[]{userName, password});
		if(Tools.isNotEmptyList(teachers)){
			return (Teacher) teachers.get(0);
		} else {
			return null;
		}
	}

}
