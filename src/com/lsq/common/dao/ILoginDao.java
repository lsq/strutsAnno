package com.lsq.common.dao;

import com.lsq.student.entity.Student;
import com.lsq.teacher.entity.Teacher;

public interface ILoginDao extends IBaseDao<Object, Integer> {
	Student validateStudent(String userName, String password);

	Teacher validateTeacher(String userName, String password);
}
