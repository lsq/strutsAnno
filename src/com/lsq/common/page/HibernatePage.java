/**
 * argasfa.java
 * create by Lisq
 * date 2014-1-9
 */
package com.lsq.common.page;

import java.util.Collections;
import java.util.List;

/**
 * @author Lisq
 * @param <T>
 */
public class HibernatePage<T> implements Page<T> {
	/** 本页包含的数据. */
	private List<T> data;
	/** 每页显示的记录数 */
	private int pageSize;
	/** 总页数. */
	private int totalPage;
	/** 当前页码. */
	private int pageNumber;
	/** 总记录数 */
	private int totalSize;

	/**
	 * 构造方法.
	 * 
	 * @param pageSize
	 *            每页显示的记录数
	 * @param pageNumber
	 *            当前页码
	 * @param recordNumber
	 *            记录总数
	 */
	public HibernatePage(int pageSize, int pageNumber, int recordNumber) {
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		this.totalPage = (int) Math.ceil((double) recordNumber
				/ (double) pageSize);
		this.totalSize = recordNumber;
		// 确定当前页
		if (this.pageNumber < 1) {
			this.pageNumber = 1;
		}
		if (this.pageNumber > this.totalPage) {
			this.pageNumber = this.totalPage;
		}
	}

	/**
	 * 构造方法：构造一个空的分页对象.
	 */
	public HibernatePage() {
		this.data = Collections.emptyList();
		this.totalPage = 0;
		this.pageNumber = 0;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public List<T> getData() {
		return this.data;
	}

	public int getTotalPage() {
		return this.totalPage;
	}

	public int getPageNumber() {
		return this.pageNumber;
	}

	public boolean hasPreviousPage() {
		return this.pageNumber > 1;
	}

	public boolean hasNextPage() {
		return this.pageNumber < this.totalPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public Long getRecordNumber() {
		return new Long(totalSize);
	}
}