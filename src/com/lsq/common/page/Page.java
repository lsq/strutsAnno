﻿package com.lsq.common.page;

import java.util.List;

public interface Page<T> {

    /**
     * 取得总页数.
     * @return  总页数
     */
    int getTotalPage();

    /**
     * 取得当前页码.
     * @return  当前页码
     */
    int getPageNumber();

    /**
     * 是否有前页.
     * @return  true --> 有前页，false --> 无前页
     */
    boolean hasPreviousPage();

    /**
     * 是否有后页.
     * @return  true --> 有后页，false --> 无后页
     */
    boolean hasNextPage();

    /**
     * 返回当前页数据.
     * @return  当前页数据
     */
    List<T> getData();

    /**
     * 设置当前页的数据.
     * @param data 要设置的数据
     */
    void setData(List<T> data);

    /**
     * 记录总条数
     * @return
     */
    Long getRecordNumber();

    /**
     * 总共页数
     * @return
     */
    int getPageSize();
}
