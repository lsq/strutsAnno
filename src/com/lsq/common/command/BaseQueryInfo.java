/**
 * QueryInfo.java
 * create by Lisq
 * date 2014-1-7
 */
package com.lsq.common.command;

import java.io.Serializable;

/**
 * @author Lisq
 */
public class BaseQueryInfo implements Serializable {

	private static final long serialVersionUID = -2787465644816242183L;

	private Integer pageNumber;

	private Integer pageSize;

	private Boolean notPage;

	public Integer getPageNumber() {
		if (pageNumber == null) {
			return new Integer(0);
		}
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Boolean getNotPage() {
		return notPage;
	}

	public void setNotPage(Boolean notPage) {
		this.notPage = notPage;
	}

	public Integer getPageSize() {
		if (pageSize == null) {
			return new Integer(5);
		}
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
